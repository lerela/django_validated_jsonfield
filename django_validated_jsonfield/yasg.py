
DEFAULT_FIELD_INSPECTORS = [
    'django_validated_jsonfield.inspectors.ValidatedJSONFieldInspector',
    'drf_yasg.inspectors.CamelCaseJSONFilter',
    'drf_yasg.inspectors.RecursiveFieldInspector',
    'drf_yasg.inspectors.ReferencingSerializerInspector',
    'drf_yasg.inspectors.ChoiceFieldInspector',
    'drf_yasg.inspectors.FileFieldInspector',
    'drf_yasg.inspectors.DictFieldInspector',
    'drf_yasg.inspectors.JSONFieldInspector',
    'drf_yasg.inspectors.HiddenFieldInspector',
    'drf_yasg.inspectors.RelatedFieldInspector',
    'drf_yasg.inspectors.SerializerMethodFieldInspector',
    'drf_yasg.inspectors.SimpleFieldInspector',
    'drf_yasg.inspectors.StringDefaultFieldInspector',
]
